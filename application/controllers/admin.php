<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        $this->load->model('mdata');
		$this->load->helper(array('form', 'url','file'));
	}
	
	function index()
	{
		$data['title'] = "Kartu Ucapan McDonalds";
		$data['description'] = "Kartu Ucapan McDonalds";
		$data['keywords'] = "Kartu Ucapan McDonalds";
		$this->load->view('vadminlogin',$data);
	}	

	function login()
	{
		$txtusername = $this->input->post("txtusername");
		$txtpassword = md5($this->input->post("txtpassword"));
		$qcekusername = $this->db->query("SELECT * FROM admin WHERE username='$txtusername' AND password='$txtpassword'");
		$valcekusername = $qcekusername->num_rows();
		if($valcekusername == 1){
			$row = $qcekusername->row();
			$userlevel = $row->level;
			$this->load->library('session');
			$this->session->set_userdata('username', $txtusername);
			$this->session->set_userdata('userlevel', $userlevel);
			redirect('admin/dashboard/','refresh');
		}else{
			$data['title'] = "Kartu Ucapan McDonalds";
			$data['description'] = "Kartu Ucapan McDonalds";
			$data['keywords'] = "Kartu Ucapan McDonalds";
			$data['error'] = "<h5 style='color:#ff0000;'>Invalid Username or Password!<br>Please Try Again!</h5>";
			$this->load->view('vadminlogin',$data);
		}
	}

	function dashboard()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = "Kartu Ucapan McDonalds";
			$data['description'] = "Kartu Ucapan McDonalds";
			$data['keywords'] = "Kartu Ucapan McDonalds";
			$data['username'] = $this->session->userdata('username');
			$this->load->view('vadmindashboard',$data);
		}else{
			redirect('ecard-admin','refresh');
		}
	}

	function sender()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = "Kartu Ucapan McDonalds";
			$data['description'] = "Kartu Ucapan McDonalds";
			$data['keywords'] = "Kartu Ucapan McDonalds";
			$data['username'] = $this->session->userdata('username');
			$data['getAllSender'] = $this->mdata->getAllSender();
			$this->load->view('vadminsender',$data);
		}else{
			redirect('ecard-admin','refresh');
		}
	}

	function detail($id)
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = "Kartu Ucapan McDonalds";
			$data['description'] = "Kartu Ucapan McDonalds";
			$data['keywords'] = "Kartu Ucapan McDonalds";
			$data['username'] = $this->session->userdata('username');
			$data['getDetail'] = $this->mdata->getDetail($id);
			$this->load->view('vadmindetail',$data);
		}else{
			redirect('ecard-admin','refresh');
		}
	}

	function logout(){
		$this->session->sess_destroy();
		redirect('ecard-admin','refresh');
	}
}