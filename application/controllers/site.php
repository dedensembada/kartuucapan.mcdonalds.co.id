<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site extends CI_Controller {

	function __construct()
    {
        parent::__construct();
    	date_default_timezone_set('Asia/Jakarta'); 
        $this->load->model('mdata');
		$this->load->library('pagination');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->helper(array('form', 'url'));			
		$this->form_validation->set_message('required', '*%s harus diisi');
		$this->form_validation->set_message('valid_email', '*Format %s salah');
	}

	public function index()
	{
		$data['title'] = 'Kartu Ucapan McDonalds';
		$this->session->unset_userdata('accessPage');
		
		// Memanggil library
		$this->load->library('recaptcha');
		$this->load->helper('captcha');

		$txtProviderName = $this->input->post('txtProviderName');
		$txtProviderEmail = $this->input->post('txtProviderEmail');
		$userCaptcha = $this->input->post('userCaptcha');
	
		$this->load->helper(array('form', 'url'));			
		$this->form_validation->set_rules('txtProviderName', 'Nama', 'required|min_length[5]|max_length[25]|xss_clean|trim');
		$this->form_validation->set_rules('txtProviderEmail', 'Email', 'required|valid_email|xss_clean|trim');
		$this->form_validation->set_rules('userCaptcha', 'Captcha', 'required|callback_check_captcha');

		if ($this->form_validation->run() == FALSE)
		{
			$data['title'] = 'Kartu Ucapan McDonalds';
			// numeric random number for captcha
	      	$random_number = substr(number_format(time() * rand(),0,'',''),0,6);
	      	// setting up captcha config
	      	$vals = array(
	             'word' => $random_number,
	             'img_path' => './assets/img/captcha/',
	             'img_url' => base_url().'assets/img/captcha/',
	             'img_width' => 140,
	             'img_height' => 32,
	             'expiration' => 7200
	            );
	      	$data['captcha'] = create_captcha($vals);
	      	$this->session->set_userdata('captchaWord',$data['captcha']['word']);
	      
			$this->load->view('vindex',$data);
		}
		else
		{
			$query = $this->db->get_where('tprovider', array('provideremail' => $txtProviderEmail));
			$count = $query->num_rows();

			//check provider if exist
			if($count == 1){
			    $date = date('Y-m-d H:i:s');
				$dataProvider = array(
					'last_login' => $date
				);
				$this->db->update('tprovider', $dataProvider);

				$this->session->set_userdata('txtSenderEmail', $txtProviderEmail);
				$this->session->set_userdata('txtSenderName', $txtProviderName);
				$this->session->set_userdata('accessPage', '1');
				redirect('penerima','refresh');
			
			}else{

			    $date = date('Y-m-d H:i:s');
				$dataProvider = array(
					'providername' => $txtProviderName,
					'provideremail' => $txtProviderEmail,
					'providerdatetime' => $date,	
					'last_login' => $date
				);
				$this->db->insert('tprovider', $dataProvider);

				$this->session->set_userdata('txtSenderEmail', $txtProviderEmail);
				$this->session->set_userdata('txtSenderName', $txtProviderName);
				$this->session->set_userdata('accessPage', '1');
				redirect('penerima','refresh');
			
			}
		}
	}

	public function check_captcha($str)
	{
	    $word = $this->session->userdata('captchaWord');
	    if(strcmp(strtoupper($str),strtoupper($word)) == 0){
	    	return true;
	    }else{
	      	$this->form_validation->set_message('check_captcha', 'Please enter correct words!');
	      	return false;
	    }
  	}

	function penerima()
	{
		if($this->session->userdata('accessPage') == 1){
			$data['title'] = 'Kartu Ucapan McDonalds';
			$providerID = $this->db->get_where('tprovider', array('provideremail' => $this->session->userdata('txtSenderEmail')))->row();
			$data['txtProvider'] = $providerID->providerid;
			$this->load->view('vpenerima',$data);
		}else{
			redirect('','refresh');
		}
	}

	function prosesPenerima()
	{
		if($this->session->userdata('accessPage') == 1){
			$data['title'] = 'Kartu Ucapan McDonalds';
			$providerid = $this->input->post('txtProvider');
			
			$txtRedeemerName1 = $this->input->post('txtRedeemerName1');
			$txtRedeemerEmail1 = $this->input->post('txtRedeemerEmail1');
			$txtRedeemerPesan1 = $this->input->post('txtRedeemerPesan1');
			$txtRedeemerCard1 = $this->input->post('txtRedeemerCard1');

			if($txtRedeemerName1 == NULL && $txtRedeemerEmail1 == NULL && $txtRedeemerPesan1 == NULL && $txtRedeemerCard1 == NULL){
				$this->form_validation->set_rules('txtRedeemerName1', 'Nama', 'required|min_length[5]|max_length[25]|xss_clean|trim');
				$this->form_validation->set_rules('txtRedeemerEmail1', 'Email', 'required|valid_email|xss_clean|trim');
				$this->form_validation->set_rules('txtRedeemerPesan1', 'Pesan', 'required|min_length[5]|max_length[75]|xss_clean|trim');
				$this->form_validation->set_rules('txtRedeemerCard1', 'Kartu', 'required|xss_clean|trim');
				if ($this->form_validation->run() == FALSE){
					$data['title'] = 'Kartu Ucapan McDonalds';
					$providerID = $this->db->get_where('tprovider', array('provideremail' => $this->session->userdata('txtSenderEmail')))->row();
					$data['txtProvider'] = $providerID->providerid;
					$this->load->view('vpenerima',$data);
					return;
				}
			}else{
				for($i=1;$i<=5;$i++){
					$txtRedeemerName = $this->input->post('txtRedeemerName'.$i);
					$txtRedeemerEmail = $this->input->post('txtRedeemerEmail'.$i);
					$txtRedeemerPesan = $this->input->post('txtRedeemerPesan'.$i);
					$txtRedeemerCard = $this->input->post('txtRedeemerCard'.$i);
					
					if($txtRedeemerName != NULL || $txtRedeemerEmail != NULL){
						$this->form_validation->set_rules('txtRedeemerName'.$i, 'Nama', 'required|min_length[5]|max_length[25]|xss_clean|trim');
						$this->form_validation->set_rules('txtRedeemerEmail'.$i, 'Email', 'required|valid_email|xss_clean|trim');
						$this->form_validation->set_rules('txtRedeemerPesan'.$i, 'Pesan', 'required|min_length[5]|max_length[75]|xss_clean|trim');
						$this->form_validation->set_rules('txtRedeemerCard'.$i, 'Kartu', 'required|xss_clean|trim');

						if ($this->form_validation->run() == FALSE){
							$data['title'] = 'Kartu Ucapan McDonalds';
							$providerID = $this->db->get_where('tprovider', array('provideremail' => $this->session->userdata('txtSenderEmail')))->row();
							$data['txtProvider'] = $providerID->providerid;
							$this->load->view('vpenerima',$data);
							return;
						}else{
							
							$txtSenderName = $this->session->userdata('$txtSenderName');
							$txtSenderEmail = $this->session->userdata('$txtSenderEmail');
							$date = date('Y-m-d H:i:s');

							//CREATE CARD IMAGE
							$path = $this->createCardImage($txtSenderEmail, $txtRedeemerEmail, $txtRedeemerName, $txtRedeemerPesan, $txtSenderName, $txtRedeemerCard);

							//EMAIL CARD
							$this->sendEmail( $path, $txtRedeemerEmail );

							//EMAIL VOUCHER
							$pathVoucher = "assets/img/voucher.jpg";
							$this->sendEmailVoucher ( $pathVoucher, $txtRedeemerEmail );

							//SAVE USER DATA
							$cardFileName = str_replace('assets/img/ecard/','',$path);
							$dataRedeemer = array(
								'redeemername' => $txtRedeemerName,
								'redeemeremail' => $txtRedeemerEmail,
								'redeemerpesan' => $txtRedeemerPesan,	
								'redeemercard' => $cardFileName,	
								'redeemerdatetime' => $date,
								'providerid' => $providerid
							);
							$this->db->insert('tredeemer', $dataRedeemer); 	
							
						}
					}
				}
				
				$this->session->unset_userdata('txtSenderName');
				$this->session->unset_userdata('txtSenderEmail');
				$this->session->set_userdata('accessPage','2');
				
				redirect('selesai','refresh');
			}
		}else{
			redirect('','refresh');
		}
	}

	function selesai()
	{
		if($this->session->userdata('accessPage') == 2){
			$data['title'] = 'Kartu Ucapan McDonalds';
			$this->load->view('vselesai',$data);
		}else{
			redirect('','refresh');
		}
	}

	function createCardImage($txtSenderEmail, $txtRedeemerEmail, $txtRedeemerName, $txtRedeemerPesan, $txtSenderName, $txtRedeemerCard){
		//FONT FAMILY
		$fontname = 'assets/fonts/AkzidenzGroteskBECn.ttf';
		
		//SET JPG QUALITY
		$quality = 100;

		//DEFINE FILE NAME & LOCATION
		//$file = "assets/img/voucher/".$this->session->userdata('vouchercode').".jpg";	
		$voucherName = 	date("Y-m-d-H-is")."-".substr(md5($this->session->userdata('txtSenderEmail')."-".$txtRedeemerEmail),0,15);
		$file = "assets/img/ecard/".$voucherName.".jpg";	

			//1. DEFINING BASE IMAGE
			$vouchersTemplate = 'assets/img/template-'.$txtRedeemerCard;
			$im = imagecreatefromjpeg($vouchersTemplate);
			
			//2. EMBED NAME PENERIMA			
			$valuename = 'Halo '.$txtRedeemerName;
			$valuefontsize = '14';
			$valuecolor = imagecolorallocate($im, 7, 109, 61);
			imagettftext($im, $valuefontsize, 0, 55, 280, $valuecolor, $fontname, $valuename);

			//2.1 EMBED PESAN
			$valuepesan = $txtRedeemerPesan;
			$valuefontsize = '14';
			$valuecolor = imagecolorallocate($im, 91, 91, 91);
			imagettftext($im, $valuefontsize, 0, 55, 310, $valuecolor, $fontname, $valuepesan);
			
			//2.2 EMBED SALAM
			$valuesalam = 'Salam,';
			$valuefontsize = '14';
			$valuecolor = imagecolorallocate($im, 91, 91, 91);
			imagettftext($im, $valuefontsize, 0, 55, 360, $valuecolor, $fontname, $valuesalam);
			
			//2.3 EMBED NAME PENGIRIM
			$valuesender = $this->session->userdata('txtSenderName');
			$valuefontsize = '14';
			$valuecolor = imagecolorallocate($im, 91, 91, 91);
			imagettftext($im, $valuefontsize, 0, 55, 390, $valuecolor, $fontname, $valuesender);

			//2.4 EMBED NAME PENERIMA PADA KARTU
			$valuename = $txtRedeemerName;
			$valuefontsize = '14';
			$valuecolor = imagecolorallocate($im, 7, 109, 61);
			imagettftext($im, $valuefontsize, 0, 77, 455, $valuecolor, $fontname, $valuename);

			//3. PRODUCE IMAGE
			imagejpeg($im, $file, 75);
	
		return $file;	
	}

	function sendEmail($path, $txtRedeemerEmail){
		$config = Array(
		    'protocol' => 'smtp',
		    'smtp_host' => 'smtp.mandrillapp.com',
		    'smtp_port' => 587,
		    'smtp_user' => 'leoburnettid.analytics@gmail.com',
		    'smtp_pass' => 'KLSAEwtDhOrIdZmxjYl9UA',
		    'mailtype'  => 'html', 
		    'charset'   => 'iso-8859-1'
		);
		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");

		$this->email->from('no-reply@mcdonalds.co.id', 'McDonalds');
		$this->email->to($txtRedeemerEmail); 

		$this->email->subject("MCDONALDS KARTU UCAPAN");
			
		$pathimage = base_url().$path;
		$messages = "
		<html>
		<head>
		<title>MCDONALDS KARTU UCAPAN</title>
		</head>
		<body>
			<a href='".base_url().$path."'>Klik disini jika image tidak terbuka</a>
			<table width='80%' style='border:none; text-align:center;'>
				<tr>
					<td style='text-align:center;'><img alt='GAMBAR KARTU' src='".base_url().$path."' width='80%'></td>
				</tr>
			</table>
		</body>
		</html>
		";

		$this->email->message($messages);
		$this->email->send();
	}

	function sendEmailVoucher($pathVoucher, $txtRedeemerEmail){
		$config = Array(
		    'protocol' => 'smtp',
		    'smtp_host' => 'smtp.mandrillapp.com',
		    'smtp_port' => 587,
		    'smtp_user' => 'leoburnettid.analytics@gmail.com',
		    'smtp_pass' => 'KLSAEwtDhOrIdZmxjYl9UA',
		    'mailtype'  => 'html', 
		    'charset'   => 'iso-8859-1'
		);
		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");

		$this->email->from('no-reply@mcdonalds.co.id', 'McDonalds');
		$this->email->to($txtRedeemerEmail); 

		$this->email->subject("MCDONALDS VOUCHER KARTU UCAPAN");
			
		$pathimage = base_url().$pathVoucher;
		$messages = "
		<html>
		<head>
		<title>MCDONALDS VOUCHER KARTU UCAPAN</title>
		</head>
		<body>
			<a href='".base_url().$pathVoucher."'>Klik disini jika image tidak terbuka</a>
			<table width='80%' style='border:none; text-align:center;'>
				<tr>
					<td style='text-align:center;'><img alt='GAMBAR KARTU' src='".base_url().$pathVoucher."' width='80%'></td>
				</tr>
			</table>
		</body>
		</html>
		";

		$this->email->message($messages);
		$this->email->send();
	}

	function imagettftextSp($image, $size, $angle, $x, $y, $color, $font, $text, $spacing = 0){        
	    if ($spacing == 0)
	    {
	        imagettftext($image, $size, $angle, $x, $y, $color, $font, $text);
	    }
	    else
	    {
	        $temp_x = $x;
	        for ($i = 0; $i < strlen($text); $i++)
	        {
	            $bbox = imagettftext($image, $size, $angle, $temp_x, $y, $color, $font, $text[$i]);
	            $temp_x += $spacing + ($bbox[2] - $bbox[0]);
	        }
	    }
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */