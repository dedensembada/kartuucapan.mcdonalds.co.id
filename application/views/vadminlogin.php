<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title><?php echo $title; ?></title>

    <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        
    <link href="<?php echo base_url(); ?>assets/css/adminstyle.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/style-responsive.css" rel="stylesheet">

  </head>

  <body>

      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->

	  <div id="login-page">
	  	<div class="container">
	  	  <div class="row">
  		    <div class="col-lg-6 col-lg-offset-3">
            <div class="clear"></div>
            <form class="form-login" action="<?php echo base_url().'admin/login';?>" method="post">
  		        <h2 class="form-login-heading">Admin Page Ecard Mcdonalds</h2>
  		        <div class="login-wrap">
  		            <input name="txtusername" type="text" class="form-control" placeholder="Username" autofocus>
  		            <br>
  		            <input name="txtpassword" type="password" class="form-control" placeholder="Password">
  		            <br>
  		            <button class="btn btn-theme btn-block" type="submit"><i class="fa fa-lock"></i> SIGN IN</button>
  		            <hr>
                  <h5><?php echo (isset($error)?$error:"")?></h5>
  		        </div>
            </form>	  
            <div class="clear"></div>	
          </div>
	  	  </div>
	  	</div>
	  </div>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

    <!--BACKSTRETCH-->
    <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.backstretch.min.js"></script>
    <script>
        $.backstretch("../../assets/img/login-bg.jpg", {speed: 500});
    </script>


  </body>
</html>
