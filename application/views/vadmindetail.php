<?php $this->load->view('vadminheader.php'); ?>
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
     <!--main content start-->
      <section id="main-content">
        <section class="wrapper">
          <h3><i class="fa fa-angle-right"></i> Penerima <a href="<?php echo base_url(); ?>admin/sender">Back</a></h3>
          <div class="row"> 
              <div class="col-lg-12">
                 <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Penerima</th>
                                        <th>Email Penerima</th>
                                        <th>Pesan</th>
                                        <th>Kartu Ucapan</th>
                                        <th>Create Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <?php 
                                    $counter=1;
                                    foreach($getDetail as $row):
                                  ?>
                                  <tr>
                                    <td><?php echo $counter; ?></td>
                                    <td><?php echo $row->redeemername; ?></td>
                                    <td><?php echo $row->redeemeremail; ?></td>
                                    <td><?php echo $row->redeemerpesan; ?></td>
                                    <td><a id="single_1" href="<?php echo base_url();?>assets/img/ecard/<?php echo $row->redeemercard;?>">
                                          <img src="<?php echo base_url().'assets/img/ecard/'.$row->redeemercard; ?>" height="100">
                                        </a>
                                    </td>
                                    <td><?php echo date('d M Y - H:i:s',strtotime($row->redeemerdatetime)); ?></td>
                                   
                                  </tr>
                                  <?php 
                                    $counter++;
                                    endforeach;
                                  ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
              </div>
          </div>          

        </section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
      <script>
      $(document).ready(function() {
        $("#single_1").fancybox({
              helpers: {
                  title : {
                      type : 'float'
                  }
              }
          });
      });
      </script>
<?php $this->load->view('vadminfooter.php'); ?>
