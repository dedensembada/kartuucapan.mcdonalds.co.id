<?php $this->load->view('vheader'); ?>

  <div class="container">
    <div class="row">
      <div class="col-lg-6 col-lg-offset-3 text-center">
        <div class="box-panel">
          <img class="thanks-icon" src="<?php echo base_url(); ?>assets/img/thanks-icon.png" alt="Kartu Ucapan McDonalds Indonesia">
          <h1>Terima Kasih,</h1>
          <h1>Kartu Ucapan Kamu Sudah Dikirimkan</h1>
          <br>
        </div>
      </div>
      <div class="col-lg-6 col-lg-offset-3 text-center">
        <br>
        <a href="<?php echo base_url();?>"><button class="form-button"><h4>KIRIM LAGI</h4></button></a>
      </div>
    </div>
  </div>
  <div class="clear"></div>
<?php $this->load->view('vfooter'); ?>