<?php $this->load->view('vadminheader.php'); ?>
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
     <!--main content start-->
      <section id="main-content">
        <section class="wrapper">
          <h3><i class="fa fa-angle-right"></i> Pengirim</h3>
          <div class="row"> 
              <div class="col-lg-12">
                 <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Create Date</th>
                                        <th>Last Login</th>
                                       
                                    </tr>
                                </thead>
                                <tbody>
                                  <?php 
                                    $counter=1;
                                    foreach($getAllSender as $row):
                                  ?>
                                  <tr>
                                    <td><?php echo $counter; ?></td>
                                    <td><a href="<?php echo base_url();?>admin/detail/<?php echo $row->providerid; ?>"><?php echo $row->providername; ?></a></td>
                                    <td><?php echo $row->provideremail; ?></td>
                                    <td><?php echo date('d M Y - H:i:s',strtotime($row->providerdatetime)); ?></td>
                                    <td><?php echo date('d M Y - H:i:s',strtotime($row->last_login)); ?></td>
                                    
                                  </tr>
                                  <?php 
                                    $counter++;
                                    endforeach;
                                  ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
              </div>
          </div>          

        </section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->

<?php $this->load->view('vadminfooter.php'); ?>
