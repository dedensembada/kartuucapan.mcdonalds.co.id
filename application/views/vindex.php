<?php $this->load->view('vheader'); ?>

  <div class="container">
    <div class="row">
      <div class="col-lg-6 col-lg-offset-3">
        <div class="box-panel">
          <h3>Pengirim</h3>
          <hr>  
          <?php echo form_open('',array('id' => 'provider-form')); ?>
          <form action="<?php echo base_url(); ?>penerima" method="post">
            <div class="form-group">
              <label>Nama</label>
              <div id="errors" style="color: #ffc425;"><?php echo form_error('txtProviderName'); ?></div>
              <input type="text" class="form-textbox" placeholder="Masukkan Nama Pengirim" name="txtProviderName" value="<?php echo set_value('txtProviderName'); ?>">
            </div>
            <div class="form-group">
              <label>Email</label>
              <span id="errors" style="color: #ffc425;"><?php echo form_error('txtProviderEmail'); ?></span>
              <input type="email" class="form-textbox" placeholder="Masukkan Email Pengirim" name="txtProviderEmail" value="<?php echo set_value('txtProviderEmail'); ?>">
            </div>
            <!-- <div class="form-group">
              <?php echo $this->recaptcha->render(); ?>
            </div> -->
            <div class="form-group">
              <label for="captcha"><?php echo $captcha['image']; ?></label>
              <br>
              <input type="text" autocomplete="off" name="userCaptcha" class="form-textbox-captcha" placeholder="Enter above text" value="<?php if(!empty($userCaptcha)){ echo $userCaptcha;} ?>" />
              <span id="errors" style="color: #ffc425;"><?php echo form_error('userCaptcha','<p id="errors" style="color:#ffc425">','</p>'); ?></span>
            </div>
          
            <div class="form-group">
              <label>Kartu ucapan kali ini adalah edisi Lebaran, dan untuk setiap penerima kartu ucapanmu akan mendapatkan pula voucher mcfloat yang bisa diredeem di cabang McDonalds tertentu.</label>
            </div>
            <br>
        </div>
      </div>
      <div class="col-lg-6 col-lg-offset-3 text-center">
        <br>
        <button type="submit" class="form-button"><h4>LANJUT</h4></button>
        </form>
      </div>
    </div>
  </div>
  <div class="clear"></div>
 
<!-- POP UP -->
<div class="popup-wrapper" id="popup">
  <div class="popup-container text-center">
    <h2>
      Terima kasih atas partisipasi Anda pada program <br>
      Kartu Ucapan Lebaran McDonalds.<br> 
      Promo telah berakhir pada 25 Juli 2015.
    </h2>
    <!-- <a class="popup-close" href="#popup">X</a> -->
  </div>
</div>
<!-- POP UP -->

<?php $this->load->view('vfooter'); ?>