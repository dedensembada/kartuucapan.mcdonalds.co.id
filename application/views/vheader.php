<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $title; ?></title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>-->
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <!--[endif]-->

    <!-- Add jQuery library -->
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>

    <!-- Add mousewheel plugin (this is optional) -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>

    <!-- Add fancyBox -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
    <script type="text/javascript" src="<?php echo base_url();?>assets/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>

    <!-- Optionally add helpers - button, thumbnail and/or media -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
    <script type="text/javascript" src="<?php echo base_url();?>assets/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

    <link rel="stylesheet" href="<?php echo base_url();?>assets/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
    <script type="text/javascript" src="<?php echo base_url();?>assets/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
  </head>
  <body>
    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <div class="col-lg-12 text-center">
          <a href="<?php echo base_url(); ?>"><img class="logo-mcd" src="<?php echo base_url(); ?>assets/img/logo-mcdonalds.jpg" alt="Kartu Ucapan McDonalds Indonesia"></a>
        </div>
      </div>
    </nav>

    <div class="container">
      <div class="row">
        <div class="col-lg-3 text-left">
          <a href="http://mcdonalds.co.id/?ref=kartuucapan" target="_blank"><h4>Kembali ke www.mcdonalds.co.id</h4></a>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-6 col-lg-offset-3 text-center">
          <br>
          <img class="logo-kartuucapan" src="<?php echo base_url(); ?>assets/img/logo-kartuucapan.png" alt="Kartu Ucapan McDonalds Indonesia">
          <br>
        </div>
      </div>
    </div>