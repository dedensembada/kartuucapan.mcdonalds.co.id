<?php $this->load->view('vheader'); ?>

  <div class="container">
    <div class="row">
      <div class="col-lg-12 box-panel">
        <h3>Penerima</h3>
        <hr>  
        <?php echo form_open('prosesPenerima',array('id' => 'redeemer-form')); ?>
          <input type="hidden" class="form-textbox" name="txtProvider" value="<?php echo $txtProvider; ?>">
          <?php for($i=1;$i<=5;$i++){ ?>
            <div class="row">
              <div class="col-lg-3">
                <div class="form-group">
                  <label>Nama </label>
                  <input type="text" class="form-textbox" placeholder="Masukkan Nama Penerima" name="txtRedeemerName<?php echo $i; ?>" value="<?php echo set_value('txtRedeemerName'.$i); ?>">
                  <div id="errors" style="color: #ffc425;"><?php echo form_error('txtRedeemerName'.$i); ?></div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="form-group">
                  <label>Email </label>
                  <input type="email" class="form-textbox" placeholder="Masukkan Email Penerima" name="txtRedeemerEmail<?php echo $i; ?>" value="<?php echo set_value('txtRedeemerEmail'.$i); ?>">
                  <div id="errors" style="color: #ffc425;"><?php echo form_error('txtRedeemerEmail'.$i); ?></div>
                </div>
              </div>
              <div class="col-lg-4">
                <div class="form-group">
                  <label>Pesan </label>
                  <input type="text" class="form-textbox" placeholder="Masukkan Pesan" name="txtRedeemerPesan<?php echo $i; ?>" value="<?php echo set_value('txtRedeemerPesan'.$i); ?>">
                  <div id="errors" style="color: #ffc425;"><?php echo form_error('txtRedeemerPesan'.$i); ?></div>
                </div>
              </div>
              <div class="col-lg-2">
                
                <div id="btn-pilih<?php echo $i; ?>">
                  <br>
                  <a class="fancybox" rel="group" href="<?php echo base_url(); ?>assets/img/ecard1.jpg" title="1">
                    <div onclick="kirim('<?php echo $i;?>' )" class="btn form-choose">PILIH KARTU</div>
                  </a>
                  <a class="fancybox" rel="group" href="<?php echo base_url(); ?>assets/img/ecard2.jpg"  title="2"></a>
                  <a class="fancybox" rel="group" href="<?php echo base_url(); ?>assets/img/ecard3.jpg" title="3"></a>
                  <a class="fancybox" rel="group" href="<?php echo base_url(); ?>assets/img/ecard4.jpg" title="4"></a>
                  <a class="fancybox" rel="group" href="<?php echo base_url(); ?>assets/img/ecard5.jpg" title="5"></a>
                  <br>
                </div>
                <div>
                  <div style="display:none;" onclick="hapus('<?php echo $i;?>' )" id="btn-cancel<?php echo $i; ?>"><img src="<?php echo base_url();?>assets/img/icon-close.png" class="btn-close"></div>
                  <div id="img-pilih<?php echo $i; ?>"></div>
                  <div id="errors" style="color: #ffc425;"><?php echo form_error('txtRedeemerCard'.$i); ?></div> 
                  <input type="hidden" class="form-textbox" name="txtRedeemerCard<?php echo $i; ?>" id="txtGambar<?php echo $i; ?>" value="<?php echo set_value('txtRedeemerCard'.$i); ?>">
                </div>
              </div>
              <div class="col-lg-12 text-center separator"></div>
             
            </div>
          
          <?php } ?>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-6 col-lg-offset-3 text-center">
        <br>
        <button type="submit" class="form-button"><h4>LANJUT</h4></button>
        </form>
      </div>
    </div>
  </div>
  <div class="clear"></div>

  <script type="text/javascript">
    var pil=0;
    var posisi;
    
    $(document).ready(function() {
      $(".fancybox").fancybox({
          openEffect  : 'elastic',
          closeEffect : 'elastic',
          openEasing  : 'swing',
          openOpacity : 'true',
          openSpeed   : 'medium',
          afterLoad: function() {
             pil=this.title;
             this.title = '<button  onclick="pilih()" class="form-button"> PILIH KARTU ' + this.title + '</button>';
          }
      });
    });

    function kirim(pos){
      posisi=pos;
    }

    function hapus(pos){
      $('#btn-pilih'+pos).show(300);
      $('#img-pilih'+pos).html('');
      $('#btn-cancel'+posisi).hide();
    }

    function pilih(){
      $.fancybox.close();

      $('#btn-pilih'+posisi).hide();
      $('#btn-cancel'+posisi).show(300);
      
      var isi= '<img class="thumbnail-img" src="<?php echo base_url(); ?>assets/img/ecard' + pil + '.jpg">';
      $('#img-pilih'+posisi).html(isi);

      $('#txtGambar'+posisi).val('ecard'+pil+'.jpg');
    }

  </script>

<?php $this->load->view('vfooter'); ?>