<?php
class Mdata extends CI_Model
{
	
    function saveProvider($dataProvider) 
    {
        $query = $this->db->insert('tprovider',$dataProvider);
        return $query->result();
    }

    function getAllSender()
    {
        $this->db->from('tprovider');
        $this->db->order_by('providerdatetime','DESC');
        $query = $this->db->get();
        return $query->result();
    }

    function getDetail($id)
    {
        $this->db->from('tredeemer');
        $this->db->where('providerid',$id);
        $this->db->order_by('redeemerdatetime','DESC');
        $query = $this->db->get();
        return $query->result();
    }

    /*
    CREATE TABLE admin ({)
        id INT NOT NULL AUTO_INCREMENT;
        username VARCHAR (255),
        password VARCHAR (255),
        PRIMARY ID (id)
    )

    INSERT INTO admin (username,password,level) VALUES ('admin','5cbb4f3f51fb749cd075e8387319a340','1');
    ALTER TABLE tprovider ADD COLUMN last_login DATETIME;

    */
}
?>
